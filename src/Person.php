<?php

namespace Adit\Phpunit;

class Person
{
    public function __construct(private string $name)
    {
    }

    public function sayHello(?string $name): string
    {
        if ($name === null) {
            throw new \Exception('Name is required');
        }

        return "Hello, $name";
    }

    public function sayGoodBye(?string $name): void {
        echo "Goodbye, $name" . PHP_EOL;
    }
}