<?php

namespace Adit\Phpunit;

class Math
{
    public static function sum(array $values): int
    {
        return array_sum($values);
    }
}