<?php declare(strict_types=1);

namespace Adit\Phpunit\Test;

use Adit\Phpunit\Counter;
use PHPUnit\Framework\TestCase;

class CounterTest extends TestCase
{
    public function testIncrement()
    {
        $counter = new Counter();
        $counter->increment();
        $this->assertEquals(1, $counter->getCounter());
    }
    
    /**
     * @test
     */
    public function increment()
    {
        $counter = new Counter();
        $counter->increment();
        $this->assertEquals(1, $counter->getCounter());
    }

    public function testDecrement()
    {
        $counter = new Counter();
        $counter->decrement();
        $this->assertEquals(-1, $counter->getCounter());
    }

    public function testOther()
    {
        echo 'Other test' . PHP_EOL ;
        $this->assertTrue(true);
    }

    public function testFirst():Counter
    {
        $counter = new Counter();
        $counter->increment();
        $counter->increment();
        $this->assertEquals(2, $counter->getCounter());

        return $counter;
    }

    /**
     * @depends testFirst
     */
    public function testSecond(Counter $counter): void
    {
        $counter->increment();
        $this->assertEquals(4, $counter->getCounter());
    }
}