<?php

use PHPUnit\Framework\TestCase;
use Adit\Phpunit\Person;

class PersonTest extends TestCase
{
    public function testSuccess()
    {
        $person = new Person('John');
        $this->assertEquals('Hello, John', $person->sayHello('John'));
    }

    public function testException()
    {
        $person = new Person('John');
        $this->expectException(\Exception::class);
        // $this->expectExceptionMessage('Name is required');

        $person->sayHello(null);
    }

    public function testOutput()
    {
        $person = new Person('John');
        $this->expectOutputString('Goodbye, John' . PHP_EOL);
        $person->sayGoodBye('John');
    }
}