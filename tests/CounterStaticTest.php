<?php

namespace Adit\Phpunit\Test;

use Adit\Phpunit\Counter;
use PHPUnit\Framework\TestCase;

class CounterStaticTest extends TestCase
{

    // this $counter will survive between tests
    public static Counter $counter;

    public static function setUpBeforeClass(): void
    {
        self::$counter = new Counter();
    }

    public function testFirst()
    {
        self::$counter->increment();
        $this->assertEquals(1, self::$counter->getCounter());
    }

    public function testSecond()
    {
        self::$counter->increment();
        $this->assertEquals(2, self::$counter->getCounter());
    }

    public function testIncrement()
    {
        self::$counter->increment();
        // TODO not complete yet
        self::markTestIncomplete('not complete yet, add assertion');
        echo 'Test Increment' . PHP_EOL;
    }

    public function testDecrement()
    {
        self::markTestSkipped('not implemented yet');
        self::$counter->decrement();
        $this->assertEquals(1, self::$counter->getCounter());
    }

    /**
     * @requires PHP >= 8.0
     * @requires OSFAMILY Linux
     */
    public function testOnlyMac()
    {
        $this->assertStringContainsString('Mac', 'MacBook');
    }

    public static function teardownAfterClass(): void
    {
        self::$counter = null;
    }
}