<?php

use PHPUnit\Framework\TestCase;
use Adit\Phpunit\Person;

class PersonTwoTest extends TestCase
{
    // $person will reset between tests
    private Person $person;

    protected function setUp(): void
    {
        // echo PHP_EOL . 'start' . PHP_EOL;
        $this->person = new Person('John');
    }

    // /**
    //  * @before
    //  */
    // public function createPerson()
    // {
    //     $this->person = new Person('John');
    // }

    public function testSuccess()
    {
        $this->assertEquals('Hello, John', $this->person->sayHello('John'));
    }

    public function testException()
    {
        $this->expectException(\Exception::class);
        // $this->expectExceptionMessage('Name is required');

        $this->person->sayHello(null);
    }

    public function testOutput()
    {
        $this->expectOutputString('Goodbye, John' . PHP_EOL);
        $this->person->sayGoodBye('John');
    }

    protected function tearDown(): void
    {
        echo 'tearDown' . PHP_EOL;
        unset($this->person);
    }

    // /**
    //  * @after
    //  */
    // public function removePerson()
    // {
    //     echo 'removePerson' . PHP_EOL;
    //     unset($this->person);
    // }
}